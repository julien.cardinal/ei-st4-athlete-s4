import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, ensemble
from sklearn.inspection import permutation_importance
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
import xlrd
import os
import numpy as np
import pandas as pd
import seaborn as sn

def dico(name):
    proteome=xlrd.open_workbook(name)
    f_proteome=proteome.sheet_by_index(0)
    dico_exposome={}
    colonnes=f_proteome.cell_value(rowx=0,colx=0).split(',')
    nblines=f_proteome.nrows
    for i in range(1,nblines):
        donnees=f_proteome.cell_value(rowx=i,colx=0).split(',')
        carac={}
        for k in range(1,len(colonnes)):
            elements=colonnes[k]
            carac[elements]=donnees[k]
        num=donnees[0]
        dico_exposome[num]=carac
    return dico_exposome

dico_exposome= dico("exposome.xls")
dico_covariates= dico("covariates.xls")
dico_phenotype= dico("phenotype.xls")
Ymaj = []
valfeature = []
continuee = 0
for val in dico_exposome["1"].keys():
    if val == '"hs_sumPCBs5_cadj_Log2"':
        break
    if val == '"hs_dde_cadj_Log2"':
        continuee = 1
    if val != '"ID"' and continuee == 1:
        X= [int(i) for i in dico_exposome.keys()]
        Y = []
        for i in X:
            valeur = dico_exposome[str(i)][val]
            if valeur == '"Undetected"':
                valeur = 0
            elif valeur == '"Detected"':
                valeur = 1
            Y.append(float(valeur))
        Ymaj.append(np.array(list(Y)))
        valfeature.append(val)


Yqi = [float(dico_phenotype[str(i)]['"hs_correct_raven"']) for i in X]
X_data, y_data = np.array(Ymaj), np.array(Yqi)
print(len(X_data))
X_data = X_data.reshape(1301,16)
print(len(y_data))
X_train, X_test, y_train, y_test = train_test_split(X_data, y_data, test_size=0.1, random_state=13)

params = {'n_estimators': 90,
          'max_depth': 10,
          'min_samples_split': 3,
          'learning_rate': 0.1,
          'loss': 'ls'}

reg = ensemble.GradientBoostingRegressor(**params)
reg.fit(X_train, y_train)

mse = mean_squared_error(y_test, reg.predict(X_test))
print("The mean squared error (MSE) on test set: {:.4f}".format(mse))

test_score = np.zeros((params['n_estimators'],), dtype=np.float64)
for i, y_pred in enumerate(reg.staged_predict(X_test)):
    test_score[i] = reg.loss_(y_test, y_pred)

fig = plt.figure(figsize=(6, 6))
plt.subplot(1, 1, 1)
plt.title('Deviance')
plt.plot(np.arange(params['n_estimators']) + 1, reg.train_score_, 'b-',
         label='Training Set Deviance')
plt.plot(np.arange(params['n_estimators']) + 1, test_score, 'r-',
         label='Test Set Deviance')
plt.legend(loc='upper right')
plt.xlabel('Boosting Iterations')
plt.ylabel('Deviance')
fig.tight_layout()
plt.show()


feature_importance = reg.feature_importances_
sorted_idx = np.argsort(feature_importance)
pos = np.arange(sorted_idx.shape[0]) + .5
fig = plt.figure(figsize=(12, 6))
plt.subplot(1, 2, 1)
plt.barh(pos, feature_importance[sorted_idx], align='center')
plt.yticks(pos, np.array(valfeature)[sorted_idx])
plt.title('Feature Importance (MDI)')

result = permutation_importance(reg, X_test, y_test, n_repeats=10,
                                random_state=42, n_jobs=2)
sorted_idx = result.importances_mean.argsort()
plt.subplot(1, 2, 2)
plt.boxplot(result.importances[sorted_idx].T,
            vert=False, labels=np.array(valfeature)[sorted_idx])
plt.title("Permutation Importance (test set)")
fig.tight_layout()
plt.show()