import xlrd
import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn

def dico(name):
    proteome=xlrd.open_workbook(name)
    f_proteome=proteome.sheet_by_index(0)
    dico_exposome={}
    colonnes=f_proteome.cell_value(rowx=0,colx=0).split(',')
    nblines=f_proteome.nrows
    for i in range(1,nblines):
        donnees=f_proteome.cell_value(rowx=i,colx=0).split(',')
        carac={}
        for k in range(1,len(colonnes)):
            elements=colonnes[k]
            carac[elements]=donnees[k]
        num=donnees[0]
        dico_exposome[num]=carac
    return dico_exposome

dico_exposome= dico("exposome.xls")
dico_covariates= dico("covariates.xls")
dico_phenotype= dico("phenotype.xls")

Ymaj = []
classe = [([],[]) for _ in range(6)]
print(classe)
valeurs_interessentes = ['"h_builtdens300_preg_Sqrt"','"h_NO2_Log"']
for val in valeurs_interessentes:
    X= [int(i) for i in dico_exposome.keys()]
    Y = [float(dico_exposome[str(i)][val]) for i in X]
    Ymaj.append(list(Y))
X= [int(i) for i in dico_covariates.keys()]
for i in X:
    if dico_covariates[str(i)]['"h_cohort"'] == '"1"':
        classe[0][0].append(float(dico_exposome[str(i)]['"h_builtdens300_preg_Sqrt"']))
        classe[0][1].append(float(dico_exposome[str(i)]['"h_NO2_Log"']))
    elif dico_covariates[str(i)]['"h_cohort"'] == '"2"':
        classe[1][0].append(float(dico_exposome[str(i)]['"h_builtdens300_preg_Sqrt"']))
        classe[1][1].append(float(dico_exposome[str(i)]['"h_NO2_Log"']))
    elif dico_covariates[str(i)]['"h_cohort"'] == '"3"':
        classe[2][0].append(float(dico_exposome[str(i)]['"h_builtdens300_preg_Sqrt"']))
        classe[2][1].append(float(dico_exposome[str(i)]['"h_NO2_Log"']))
    elif dico_covariates[str(i)]['"h_cohort"'] == '"4"':
        classe[3][0].append(float(dico_exposome[str(i)]['"h_builtdens300_preg_Sqrt"']))
        classe[3][1].append(float(dico_exposome[str(i)]['"h_NO2_Log"']))
    elif dico_covariates[str(i)]['"h_cohort"'] == '"5"':
        classe[4][0].append(float(dico_exposome[str(i)]['"h_builtdens300_preg_Sqrt"']))
        classe[4][1].append(float(dico_exposome[str(i)]['"h_NO2_Log"']))
    elif dico_covariates[str(i)]['"h_cohort"'] == '"6"':
        classe[5][0].append(float(dico_exposome[str(i)]['"h_builtdens300_preg_Sqrt"']))
        classe[5][1].append(float(dico_exposome[str(i)]['"h_NO2_Log"']))
colors = ["red","blue","green","crimson","cyan","darkorange"]
for j in range(6):
    plt.plot(classe[j][0],classe[j][1],linestyle="None",marker="o")#,markerfacecolor=colors[j])
    
print(len(classe[1][0]))
plt.legend("123456")
plt.title("Clustering de la polution de l'air en fonction du lieu de résidence")
plt.show()


