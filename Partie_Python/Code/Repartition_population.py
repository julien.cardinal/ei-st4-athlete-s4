import xlrd
import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn

def dico(name):
    proteome=xlrd.open_workbook(name)
    f_proteome=proteome.sheet_by_index(0)
    dico_exposome={}
    colonnes=f_proteome.cell_value(rowx=0,colx=0).split(',')
    nblines=f_proteome.nrows
    for i in range(1,nblines):
        donnees=f_proteome.cell_value(rowx=i,colx=0).split(',')
        carac={}
        for k in range(1,len(colonnes)):
            elements=colonnes[k]
            carac[elements]=donnees[k]
        num=donnees[0]
        dico_exposome[num]=carac
    return dico_exposome

dico_exposome= dico("exposome.xls")
dico_covariates= dico("covariates.xls")
dico_phenotype= dico("phenotype.xls")


print(len(dico_covariates))
print(dico_covariates['880'])

def bubblesort(list1,list2):

# Swap the elements to arrange in order
    for iter_num in range(len(list1)-1,0,-1):
        for idx in range(iter_num):
            if list2[idx]>list2[idx+1]:
                temp = list1[idx]
                list1[idx] = list1[idx+1]
                list1[idx+1] = temp
pas = 20

Ymaj = {}
liste_interessante = ['"hs_as_c_Log2"','"hs_as_m_Log2"','"hs_cd_c_Log2"','"hs_cd_m_Log2"','"hs_co_c_Log2"','"hs_co_m_Log2"','"hs_cs_c_Log2"','"hs_cs_m_Log2"','"hs_cu_c_Log2"','"hs_cu_m_Log2"','"hs_hg_c_Log2"','"hs_hg_m_Log2"','"hs_mn_c_Log2"','"hs_mn_m_Log2"','"hs_mo_c_Log2"','"hs_mo_m_Log2"','"hs_pb_c_Log2"','"hs_pb_m_Log2"','"hs_tl_c_Log2"','"hs_tl_m_Log2"']
continuee = 0
for val in dico_exposome["1"].keys():
    if val == '"hs_sumPCBs5_cadj_Log2"':
        break
    if val == '"hs_dde_cadj_Log2"':
        continuee = 1
    if val != '"ID"' and continuee==1:
        print(val,dico_exposome['1'][val])
        X= [int(i) for i in dico_exposome.keys()]
        Y = []
        for i in X:
            valeur = dico_exposome[str(i)][val]
            if valeur == '"Undetected"':
                valeur = 0
            elif valeur == '"Detected"':
                valeur = 1
            elif valeur == '"2"':
                valeur = 2
            elif valeur == '"3"':
                valeur = 3
            elif valeur == '"4"':
                valeur = 4
            elif valeur == '"Yes"':
                valeur = 1
            elif valeur == '"No"':
                valeur = 0
            print(valeur)
            Y.append(float(valeur))
        Ypoids = [float(dico_phenotype[str(i)]['"e3_bw"']) for i in X]
        Ymaj[val] = list(Y)
        mini = min(Y)
        maxi = max(Y)
        """
        print(mini,maxi,np.mean(Y))
        XX = [0]*101

        for y in Y:
            XX[int((y-mini)*100/(maxi-mini))] += 1 #XX nb dans case
        XXX = [(y*(maxi-mini)/100)+mini for y in range(len(XX))]
        plt.title("Le nombre de personnes par concentration en " + val)
        plt.plot(XXX,XX)
        plt.show()
        """
        # moyenne sur 5

        bubblesort(Ypoids,Y)
        Y.sort()
        Ymoy = []
        Xmoy = []
        part = 1
        nb = 0
        while nb<len(Y):
            summ = 0
            nb_part = 0
            while nb<len(Y) and Y[nb]<part*(maxi-mini)/pas+mini:
                summ += Ypoids[nb]
                nb_part += 1
                nb += 1
            if nb_part != 0:
                Ymoy.append(summ/nb_part)
                Xmoy.append((part-0.5)*(maxi-mini)/pas+mini)
            part += 1


        """ 
        plt.title("Le poids de l'enfant à l'enfance en fonction de " + val)
        plt.plot(Y,Ypoids,linestyle="None",marker="x")
        plt.plot(Xmoy,Ymoy)
        plt.show()
        """

for val in dico_covariates["1"].keys():
    if val == '"h_edumc_None"':
        break
    if val not in  ['"ID"','"h_cohort"', '"e3_sex_None"', '"e3_yearbir_None"']:
        X= [int(i) for i in dico_covariates.keys()]
        Y = [float(dico_covariates[str(i)][val]) for i in X]
        Ypoids = [float(dico_phenotype[str(i)]['"e3_bw"']) for i in X]
        Ymaj[val] = list(Y)
        mini = min(Y)
        maxi = max(Y)
        """
        print(mini,maxi,np.mean(Y))
        XX = [0]*101

        for y in Y:
            XX[int((y-mini)*100/(maxi-mini))] += 1 #XX nb dans case
        XXX = [(y*(maxi-mini)/100)+mini for y in range(len(XX))]
        plt.title("Le nombre de personnes par concentration en " + val)
        plt.plot(XXX,XX)
        plt.show()
        """
        # moyenne sur 5

        bubblesort(Ypoids,Y)
        Y.sort()
        Ymoy = []
        Xmoy = []
        part = 1
        nb = 0
        while nb<len(Y):
            summ = 0
            nb_part = 0
            while nb<len(Y) and Y[nb]<part*(maxi-mini)/pas+mini:
                summ += Ypoids[nb]
                nb_part += 1
                nb += 1
            if nb_part != 0:
                Ymoy.append(summ/nb_part)
                Xmoy.append((part-0.5)*(maxi-mini)/pas+mini)
            part += 1


            
        plt.title("Le poids de l'enfant à l'enfance en fonction de " + val)
        plt.plot(Y,Ypoids,linestyle="None",marker="x")
        plt.plot(Xmoy,Ymoy)
        plt.show()

for val in dico_phenotype["1"].keys():
    if val == '"hs_bmi_c_cat"':
        break
    if val not in  ['"ID"','"h_cohort"', '"e3_sex_None"', '"e3_yearbir_None"']:
        X= [int(i) for i in dico_phenotype.keys()]
        Y = [float(dico_phenotype[str(i)][val]) for i in X]
        Ymaj[val] = list(Y)

df = pd.DataFrame(Ymaj)

corrMatrix = df.corr()
sn.heatmap(corrMatrix, annot=True)
plt.show()
print(corrMatrix)